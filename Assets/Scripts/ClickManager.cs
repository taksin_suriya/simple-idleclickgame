using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ClickManager : MonoBehaviour
{
    // List to store timestamps of auto-clickers
    private List<float> autoClickersLastTime = new List<float>();

    [SerializeField] private int autoClickPrice = 50; // Serialize for inspector access

    private float autoClickInterval = 1.0f; // Time interval for auto-clicks

    public TextMeshProUGUI amountBuyText;

    private void Update()
    {
        // Use a reversed loop to avoid issues when removing elements
        for (int i = autoClickersLastTime.Count - 1; i >= 0; i--)
        {
            float lastClickTime = autoClickersLastTime[i];

            // Check if it's time to trigger an auto-click
            if (Time.time - lastClickTime >= autoClickInterval)
            {
                // Update the timestamp to the current time
                autoClickersLastTime[i] = Time.time;

                // Damage the current enemy (presumably in a game)
                EnemyManager.Instance.currentEnemy.Damage();
            }
        }
        amountBuyText.text = "x" + autoClickersLastTime.Count.ToString();
    }

    // This method is called when the player wants to buy an auto-clicker
    public void OnBuyAutoClicker()
    {
        GameManager gameManager = GameManager.Instance;
        
        
        // Check if the player has enough gold to purchase
        if (gameManager.gold >= autoClickPrice)
        {
            // Deduct the auto-clicker price from the player's gold
            gameManager.TakeGold(autoClickPrice);

            // Add the current time as a timestamp for the new auto-clicker
            autoClickersLastTime.Add(Time.time);
        }
    }
}
