using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    [SerializeField] public float damageAmount; // Damage dealt by the enemy
    public float currentHealth; // Current health of the enemy
    public float maxHealth = 100f; // Maximum health of the enemy
    public int goldToGive = 10; // Amount of gold to give when the enemy is defeated
    public Image healthbar; // Reference to a UI health bar image
    public Animation anim;

    private void Start()
    {
        currentHealth = maxHealth; // Initialize current health to maximum health
        healthbar.fillAmount = maxHealth; // Set the initial fill amount of the health bar
    }

    // Method to handle damage to the enemy
    public void Damage()
    {
        currentHealth -= damageAmount; // Reduce current health by the damage amount
        healthbar.fillAmount = currentHealth / maxHealth; // Update the health bar UI
        anim.Stop();
        anim.Play();

        if (currentHealth <= 0)
        {
            Dead(); // If health reaches zero or less, call the Dead() method
        }
    }

    // Method to handle enemy death
    public void Dead()
    {
        GameManager.Instance.AddGold(goldToGive); // Add gold to the player's inventory
        EnemyManager.Instance.DefeatEnemy(gameObject); // Notify the EnemyManager that this enemy is defeated
        Debug.Log("Enemy die"); // Log a message to the console indicating the enemy's death
    }
}
