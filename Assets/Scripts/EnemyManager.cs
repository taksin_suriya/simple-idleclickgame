using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject[] enemyPrefab; // Array of enemy prefabs to spawn
    public Enemy currentEnemy; // Reference to the currently active enemy
    public Transform canvas; // Reference to the canvas where enemies are spawned

    public static EnemyManager Instance { get; private set; } // Singleton instance of the EnemyManager

    private void Awake()
    {
        // Create a singleton instance of the EnemyManager
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Method to create a new enemy
    public void CreateNewEnemy()
    {
        // Randomly select an enemy prefab from the array
        GameObject enemyToSpawn = enemyPrefab[Random.Range(0, enemyPrefab.Length)];

        // Instantiate the selected enemy prefab on the canvas
        GameObject obj = Instantiate(enemyToSpawn, canvas);

        // Get the Enemy component from the spawned enemy
        currentEnemy = obj.GetComponent<Enemy>();
    }

    // Method to handle enemy defeat
    public void DefeatEnemy(GameObject enemy)
    {
        // Destroy the defeated enemy GameObject
        Destroy(enemy);

        // Create a new enemy to replace the defeated one
        CreateNewEnemy();

        // Notify the GameManager to check for background changes
        GameManager.Instance.BackgroundCheck();
    }
}
