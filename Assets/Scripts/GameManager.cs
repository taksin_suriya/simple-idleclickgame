using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int gold; // Player's gold
    public TextMeshProUGUI goldText; // Reference to a TextMeshProUGUI for displaying gold
    public Sprite[] background; // Array of background sprites
    private int curBackground; // Index of the current background sprite
    private int changeBackgroundCount = 5; // Counter for changing the background
    public Image backgroundImage; // Reference to an Image for displaying backgrounds
    public static GameManager Instance { get; private set; } // Singleton instance of the GameManager

    private void Awake()
    {
        // Create a singleton instance of the GameManager
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Method to add gold to the player's inventory
    public void AddGold(int amount)
    {
        gold += amount;
        goldText.text = "Gold:" + gold.ToString();
    }

    // Method to deduct gold from the player's inventory
    public void TakeGold(int amount)
    {
        gold -= amount;
        goldText.text = "Gold:" + gold.ToString();
    }

    // Method to change the background after a certain number of times
    public void BackgroundCheck()
    {
        changeBackgroundCount--;

        if (changeBackgroundCount == 0)
        {
            changeBackgroundCount = 5; // Reset the counter

            curBackground++; // Move to the next background

            if (curBackground == background.Length)
            {
                curBackground = 0; // Wrap around if the end of the array is reached
            }

            backgroundImage.sprite = background[curBackground]; // Update the background image
        }
    }
}
